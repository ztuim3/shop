import React, {useEffect, useState} from 'react';
import {Text, View, FlatList} from 'react-native';
import {NavigationContainer, useIsFocused} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';
import StoreScreen from './components/StoreScreen';
import LoginScreen from './components/LoginScreen';
import RegisterScreen from './components/RegisterScreen';
import AdminScreen from './components/AdminScreen';

function CartScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>
        <Text>Cart!!!</Text>
      </Text>
    </View>
  );
}

function OrderedScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Ordered!</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

export default function App() {
  const [token, setToken] = React.useState();

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    setToken(
      AsyncStorage.getItem('TOKEN')
        .then(asyncStorageRes => {
          console.log(JSON.parse(asyncStorageRes));
          return JSON.parse(asyncStorageRes);
        })
        .catch(err => console.log(err)),
    );
  }, []);

  //const [user, setUser] = useState('guest');

  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Store" component={StoreScreen} />
        {!token ? <Tab.Screen name="Cart" component={CartScreen} /> : null}
        <Tab.Screen name="Ordered" component={OrderedScreen} />
        <Tab.Screen name="Login" component={LoginScreen} />
        <Tab.Screen name="Register" component={RegisterScreen} />
        <Tab.Screen name="Admin" component={AdminScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
