import React from 'react';
import {
  Text,
  TextInput,
  StyleSheet,
  Button,
  View,
  ScrollView,
} from 'react-native';
import {AlertDialog} from '../AlertPopUp/Alert';
import useMyContext from '../App/appContext';

const AddProductScreen = ({navigation}) => {
  const [title, onChangeTitle] = React.useState('');
  const [brand, onChangeBrand] = React.useState('');
  const [hardDiskSize, onChangeHardDiskSize] = React.useState('');
  const [cpuModel, onChangeCpuModel] = React.useState('');
  const [ramMemory, onChangeRamMemory] = React.useState('');
  const [graphicsProcessor, onChangeGraphicsProcessor] = React.useState('');
  const {token} = useMyContext();

  const onPressCreate = async () => {
    fetch('http://localhost:3000/api/products', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        title: title,
        brand: brand,
        hardDiskSize: hardDiskSize,
        cpuModel: cpuModel,
        ramMemory: ramMemory,
        graphicsProcessor: graphicsProcessor,
      }),
    })
      .then(response => response.json())
      .then(json => {
        AlertDialog(
          'Product added',
          'The product has been successfully added to the store',
        );
      })
      .catch(error => {
        AlertDialog('Something goes wrong', 'You must enter all the data.');
      })
      .finally(() => {
        navigation.navigate('Store');
      });
  };

  return (
    <ScrollView>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Text>Title:</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeTitle}
          value={title}
        />
        <Text>Brand:</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeBrand}
          value={brand}
        />
        <Text>Hard Disk Size:</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeHardDiskSize}
          value={hardDiskSize}
        />
        <Text>CPU Model:</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeCpuModel}
          value={cpuModel}
        />
        <Text>RAM Memory:</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeRamMemory}
          value={ramMemory}
        />
        <Text>Graphics Processor:</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeGraphicsProcessor}
          value={graphicsProcessor}
        />
        <Button onPress={onPressCreate} title="Create" color="#841584" />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: 16,
    alignSelf: 'center',
  },

  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

export default AddProductScreen;
