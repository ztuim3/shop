import React, {useEffect, useState} from 'react';
import {View, FlatList, StyleSheet, SafeAreaView, Text} from 'react-native';
import Item from '../ListProducts/Item';
import {onPressRemoveFromShop} from '../ListProducts/handleOnPress';

function AdminScreen({navigation}) {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3000/api/products/')
      .then(response => response.json())
      .then(json => {
        setData(json);
      })
      .catch(error => console.error(error));
  }, [data]);

  const renderItem = ({item}) => (
    <Item
      id={item._id}
      title={item.title}
      brand={item.brand}
      hardDiskSize={item.hardDiskSize}
      cpuModel={item.cpuModel}
      ramMemory={item.ramMemory}
      graphicsProcessor={item.graphicsProcessor}
      createdBy={item.createdBy}
      status={item.status}
      orderedBy={item.orderedBy}
      createdAt={item.createdAt}
      updatedAt={item.updatedAt}
      city={item.city}
      street={item.street}
      number={item.number}
      navigation={navigation}
      fetchFunction={onPressRemoveFromShop}
      buttonTitle="Remove from Store"
      routeAddress="Admin"
    />
  );

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <Text style={styles.title}>Status of all products</Text>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item._id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 22,
    alignSelf: 'center',
  },
});

export default AdminScreen;
