import {Alert} from 'react-native';

export const AlertDialog = (title, text) => {
  return Alert.alert(title, text, [
    {
      text: 'Ok',
    },
  ]);
};
