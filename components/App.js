import React from 'react';
import {AppProvider} from './App/appContext';
import Navigation from './Navigation/Navigation';

export default function App() {
  return (
    <AppProvider>
      <Navigation />
    </AppProvider>
  );
}
