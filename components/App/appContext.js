import React, {createContext, useContext, useReducer, useMemo} from 'react';
import appReducer, {initialState} from './appReducer';

const AppContext = createContext(initialState);

export const AppProvider = ({children}) => {
  const [state, dispatch] = useReducer(appReducer, initialState);

  const setToken = (token, privilege, email) => {
    dispatch({
      type: 'LOGIN',
      payload: {
        token: token,
        privilege: privilege,
        email: email,
      },
    });
  };

  const removeToken = () => {
    dispatch({
      type: 'LOGOUT',
      payload: {
        token: null,
        privilege: null,
        email: null,
      },
    });
  };

  const value = {
    token: state.token,
    privilege: state.privilege,
    email: state.email,
    setToken,
    removeToken,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

const useMyContext = () => {
  const context = useContext(AppContext);

  if (context == undefined) {
    throw new Error(`useMyContext must be used within AppContext`);
  }

  return context;
};

export default useMyContext;
