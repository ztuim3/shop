export const initialState = {
  token: null,
  privilege: null,
  email: null,
};

const appReducer = (state, action) => {
  const {type, payload} = action;

  switch (type) {
    case 'LOGIN':
      console.log('LOGIN', payload);
      return {
        ...state,
        token: payload.token,
        privilege: payload.privilege,
        email: payload.email,
      };
    case 'LOGOUT':
      console.log('LOGOUT', payload);
      return {
        ...state,
        token: payload.token,
        privilege: payload.privilege,
        email: payload.email,
      };
    default:
      throw new Error(`No case for type ${type} in appReducer`);
  }
};

export default appReducer;
