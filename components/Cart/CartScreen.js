import React, {useEffect, useState} from 'react';
import {Text, View, FlatList, StyleSheet} from 'react-native';
import ModalPopUp from './ModalPopUp';
import {onPressRemoveFromCart} from '../ListProducts/handleOnPress';
import Item from '../ListProducts/Item';
import useMyContext from '../App/appContext';

function CartScreen({navigation, route}) {
  const [data, setData] = useState([]);
  const {token, privilege, email} = useMyContext();

  useEffect(() => {
    fetch('http://localhost:3000/api/products/inCart/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(json => {
        setData(json);
      })
      .catch(error => console.error(error));
  }, [data, token]);

  const renderItem = ({item}) => (
    <Item
      id={item._id}
      title={item.title}
      brand={item.brand}
      hardDiskSize={item.hardDiskSize}
      cpuModel={item.cpuModel}
      ramMemory={item.ramMemory}
      graphicsProcessor={item.graphicsProcessor}
      createdBy={item.createdBy}
      status={item.status}
      orderedBy={item.orderedBy}
      createdAt={item.createdAt}
      updatedAt={item.updatedAt}
      city={item.city}
      street={item.street}
      number={item.number}
      navigation={navigation}
      fetchFunction={onPressRemoveFromCart}
      buttonTitle="Remove from Cart"
      routeAddress="Cart"
      token={token}
      privilege={privilege}
      email={email}
    />
  );

  return (
    <>
      {data.length !== 0 ? (
        <>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={item => item._id}
            />
            <ModalPopUp />
          </View>
        </>
      ) : (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>You have nothing in your cart.</Text>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default CartScreen;
