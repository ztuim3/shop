import React, {useState} from 'react';
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  TextInput,
} from 'react-native';
import GetLocation from 'react-native-get-location';
import useMyContext from '../App/appContext';

const ModalPopUp = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [city, onChangeCity] = React.useState();
  const [street, onChangeStreet] = React.useState();
  const [number, onChangeNumber] = React.useState();
  const {token, privilege, email, setToken, removeToken} = useMyContext();

  const onPressGetLocation = async () => {
    let longitude;
    let latitude;

    await GetLocation.getCurrentPosition({
      enableHighAccuracy: false,
      timeout: 15000,
    })
      .then(location => {
        longitude = location.longitude;
        latitude = location.latitude;
        console.log(longitude, latitude);
        fetchStreet(location.latitude, location.longitude);
      })
      .catch(error => {
        const {code, message} = error;
        console.warn(code, message);
      });
  };

  const fetchStreet = async (latitude, longitude) => {
    await fetch(
      `https://api.geoapify.com/v1/geocode/reverse?lat=${latitude}&lon=${longitude}&apiKey=24a91db1e6624f3eaeea441d9c3929b5`,
      {
        method: 'GET',
      },
    )
      .then(response => response.json())
      .then(result => {
        onChangeCity(result.features[0].properties.city);
        onChangeStreet(result.features[0].properties.street);
        onChangeNumber(result.features[0].properties.housenumber);
      })
      .catch(error => console.log('error', error));
  };

  const onPressCreate = async () => {
    fetch(`http://localhost:3000/api/products/updateSubmitOrder/${email}`, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        city: city,
        street: street,
        number: number,
      }),
    })
      .then(response => response.json())
      .then(json => {
        Alert.alert(
          'Ordered',
          'Your order will be processed within a few days. Thank you for your trust',
          [
            {
              text: 'OK',
              onPress: () => setModalVisible(!modalVisible),
            },
          ],
        );
      })
      .catch(error => {
        console.error(error);
        Alert.alert('Something goes wrong', 'You must enter all the data', [
          {
            text: 'OK',
            onPress: () => setModalVisible(!modalVisible),
          },
        ]);
      });
  };

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text>Enter your address to confirm your order</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={onPressGetLocation}>
              <Text style={styles.textStyle}>Get your location</Text>
            </Pressable>
            <Text>City</Text>
            <TextInput
              style={styles.input}
              onChangeText={onChangeCity}
              value={city}
            />
            <Text>Street</Text>
            <TextInput
              style={styles.input}
              onChangeText={onChangeStreet}
              value={street}
            />
            <Text>Number</Text>
            <TextInput
              style={styles.input}
              onChangeText={onChangeNumber}
              value={number}
            />
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={onPressCreate}>
              <Text style={styles.textStyle}>Confirm your order</Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}>
              <Text style={styles.textStyle}>Discard</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <Pressable
        style={[styles.button, styles.buttonClose]}
        onPress={() => setModalVisible(true)}>
        <Text style={styles.textStyle}>Order</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    padding: 20,
    elevation: 2,
    margin: 10,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  input: {
    height: 40,
    marginBottom: 5,
    borderWidth: 1,
    padding: 10,
  },
});

export default ModalPopUp;
