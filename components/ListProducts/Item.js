import React from 'react';
import {Text, StyleSheet, Button, View} from 'react-native';

const Item = ({
  id,
  title,
  brand,
  hardDiskSize,
  cpuModel,
  ramMemory,
  graphicsProcessor,
  createdBy,
  status,
  orderedBy,
  createdAt,
  updatedAt,
  city,
  street,
  number,
  navigation,
  fetchFunction,
  buttonTitle,
  routeAddress,
  token,
  privilege,
  email,
}) => (
  <View style={styles.item}>
    <Text style={styles.title}>Title:{title}</Text>
    <Text>Brand: {brand}</Text>
    <Text>Hard Disk Size: {hardDiskSize} GB</Text>
    <Text>CPU Model: {cpuModel}</Text>
    <Text>RAM Memory: {ramMemory} GB</Text>
    <Text>Graphics Processor: {graphicsProcessor}</Text>
    {routeAddress == 'Admin' ? <Text>ID:{id}</Text> : null}
    {routeAddress == 'Admin' ? <Text>Created by:{createdBy}</Text> : null}
    <Text>Status: {status}</Text>
    {routeAddress == 'Admin' ? <Text>Ordered by:{orderedBy}</Text> : null}
    {routeAddress == 'Admin' ? <Text>Created at:{createdAt}</Text> : null}
    {routeAddress == 'Admin' ? <Text>Change status at:{updatedAt}</Text> : null}
    {routeAddress == 'Admin' || routeAddress == 'Ordered' ? (
      <Text>City:{city}</Text>
    ) : null}
    {routeAddress == 'Admin' || routeAddress == 'Ordered' ? (
      <Text>Street:{street}</Text>
    ) : null}
    {routeAddress == 'Admin' || routeAddress == 'Ordered' ? (
      <Text>Number:{number}</Text>
    ) : null}
    {routeAddress !== 'Ordered' ? (
      <Button
        onPress={() => fetchFunction(id, navigation, token)}
        title={buttonTitle}
        style={styles.button}
      />
    ) : null}
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
    marginBottom: 10,
  },
  button: {
    color: '#841584',
    marginBottom: 10,
  },
});

export default Item;
