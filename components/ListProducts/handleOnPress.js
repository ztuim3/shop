import {AlertDialog} from '../AlertPopUp/Alert';

export const onPressRemoveFromShop = async (id, navigation, token) => {
  fetch(`http://localhost:3000/api/products/${id}`, {
    method: 'DELETE',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(response => response.json())
    .then(json => {
      AlertDialog('Removed', 'You have removed a product from the store');
    })
    .catch(error => {
      AlertDialog('Something goes wrong', 'You are not logged');
    })
    .finally(() => {
      navigation.navigate('Store');
    });
};

export const onPressAddToCart = async (id, navigation, token) => {
  fetch(`http://localhost:3000/api/products/updateInCart/${id}`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(response => response.json())
    .then(json => {
      AlertDialog('Added', 'You have added the product to the cart');
    })
    .catch(error => {
      AlertDialog('Something goes wrong', 'You are not logged');
    })
    .finally(() => {
      navigation.navigate('Store');
    });
};

export const onPressRemoveFromCart = async (id, navigation, token) => {
  fetch(`http://localhost:3000/api/products/updateBackToStore/${id}`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(response => response.json())
    .then(json => {
      AlertDialog('Removed', 'You have removed a product from the cart');
    })
    .catch(error => {
      AlertDialog('Something goes wrong', 'You need to log in again');
    })
    .finally(() => {
      navigation.navigate('Store');
    });
};
