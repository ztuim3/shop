import React from 'react';
import {Text, View, TextInput, StyleSheet, Button} from 'react-native';
import {AlertDialog} from '../AlertPopUp/Alert';
import useMyContext from '../App/appContext';

function LoginScreen({navigation}) {
  const [login, onChangeLogin] = React.useState('');
  const [password, onChangePassword] = React.useState('');
  const {token, privilege, email, setToken, removeToken} = useMyContext();

  const onPressLogin = () => {
    fetch('http://localhost:3000/api/auth/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: login,
        password: password,
      }),
    })
      .then(response => response.json())
      .then(json => {
        setToken(json.token, json.privilege, json.email);
        AlertDialog(
          'Logged',
          'You have successfully logged in to the application',
        );
      })
      .catch(error => {
        AlertDialog(
          'Something goes wrong',
          'Check that you have entered the data correctly',
        );
      })
      .finally(() => navigation.navigate('Store'));
  };

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <Text>Login:</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangeLogin}
        value={login}
      />
      <Text>Password:</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangePassword}
        value={password}
      />
      <Button onPress={onPressLogin} title="Login" color="#841584" />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

export default LoginScreen;
