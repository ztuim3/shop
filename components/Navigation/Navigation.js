import React from 'react';
import {Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import StoreScreen from '../Store/StoreScreen';
import LoginScreen from '../Login/LoginScreen';
import RegisterScreen from '../Register/RegisterScreen';
import AdminScreen from '../Admin/AdminScreen';
import CartScreen from '../Cart/CartScreen';
import OrderedScreen from '../Ordered/OrderedScreen';
import AddProductScreen from '../Admin/AddProductScreen';
import useMyContext, {AppProvider} from '../App/appContext';

const Tab = createBottomTabNavigator();

export default function Navigation() {
  const {token, privilege, email, setToken, removeToken} = useMyContext();

  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Store" component={StoreScreen} />
        {!token ? <Tab.Screen name="Login" component={LoginScreen} /> : null}
        {!token ? (
          <Tab.Screen name="Register" component={RegisterScreen} />
        ) : null}
        {token ? (
          <Tab.Screen
            name="Cart"
            component={CartScreen}
            options={{
              headerRight: () => (
                <Button onPress={() => removeToken()} title="Logout" />
              ),
            }}
          />
        ) : null}
        {token ? <Tab.Screen name="Ordered" component={OrderedScreen} /> : null}
        {privilege == 'Admin' ? (
          <Tab.Screen name="Admin" component={AdminScreen} />
        ) : null}
        {privilege == 'Admin' ? (
          <Tab.Screen name="AddProduct" component={AddProductScreen} />
        ) : null}
      </Tab.Navigator>
    </NavigationContainer>
  );
}

// export default function App() {
//   return <Text>Siema</Text>;
// }
