import React, {useEffect, useState} from 'react';
import {View, FlatList, StyleSheet, Text} from 'react-native';
import Item from '../ListProducts/Item';
import useMyContext from '../App/appContext';

function OrderedScreen({navigation, route}) {
  const [data, setData] = useState([]);
  const {token, privilege, email, setToken, removeToken} = useMyContext();

  useEffect(() => {
    fetch('http://localhost:3000/api/products/ordered/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(json => {
        setData(json);
      })
      .catch(error => console.error(error));
  }, [data]);

  const renderItem = ({item}) => (
    <Item
      id={item._id}
      title={item.title}
      brand={item.brand}
      hardDiskSize={item.hardDiskSize}
      cpuModel={item.cpuModel}
      ramMemory={item.ramMemory}
      graphicsProcessor={item.graphicsProcessor}
      createdBy={item.createdBy}
      status={item.status}
      orderedBy={item.orderedBy}
      createdAt={item.createdAt}
      updatedAt={item.updatedAt}
      city={item.city}
      street={item.street}
      number={item.number}
      navigation={navigation}
      routeAddress="Ordered"
      token={token}
      privilege={privilege}
      email={email}
    />
  );

  return (
    <>
      {data.length !== 0 ? (
        <>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={item => item._id}
            />
          </View>
        </>
      ) : (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>You haven't ordered anything in our store yet.</Text>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default OrderedScreen;
