import React from 'react';
import {Text, View, TextInput, StyleSheet, Button} from 'react-native';
import {AlertDialog} from '../AlertPopUp/Alert';

function RegisterScreen({navigation}) {
  const [login, onChangeLogin] = React.useState('');
  const [password, onChangePassword] = React.useState('');
  const [firstName, onChangeFirst] = React.useState('');
  const [lastName, onChangeLast] = React.useState('');

  const onPressRegister = () => {
    fetch('http://localhost:3000/api/auth/register', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        first_name: firstName,
        last_name: lastName,
        email: login,
        password: password,
      }),
    })
      .then(response => response.json())
      .then(json => {
        AlertDialog(
          'Registered',
          'You have successfully registered for the application',
        );
      })
      .catch(error => {
        AlertDialog(
          'Something goes wrong',
          'Check that you have entered the data correctly',
        );
      })
      .finally(() => navigation.navigate('Store'));
  };

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <Text>First Name:</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangeFirst}
        value={firstName}
      />
      <Text>Last Name:</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangeLast}
        value={lastName}
      />
      <Text>Login:</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangeLogin}
        value={login}
      />
      <Text>Password:</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangePassword}
        value={password}
      />
      <Button onPress={onPressRegister} title="Register" color="#841584" />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

export default RegisterScreen;
