import React, {useEffect, useState} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import Item from '../ListProducts/Item';
import {onPressAddToCart} from '../ListProducts/handleOnPress';
import useMyContext from '../App/appContext';
import {useIsFocused} from '@react-navigation/native';

function StoreScreen({navigation}) {
  const [data, setData] = useState([]);
  const {token, privilege, email, setToken, removeToken} = useMyContext();
  const isFocused = useIsFocused();

  useEffect(() => {
    fetch('http://localhost:3000/api/products/inStore/')
      .then(response => response.json())
      .then(json => {
        setData(json);
      })
      .catch(error => console.error(error));
  }, [data, isFocused]);

  const renderItem = ({item}) => (
    <Item
      id={item._id}
      title={item.title}
      brand={item.brand}
      hardDiskSize={item.hardDiskSize}
      cpuModel={item.cpuModel}
      ramMemory={item.ramMemory}
      graphicsProcessor={item.graphicsProcessor}
      createdBy={item.createdBy}
      status={item.status}
      orderedBy={item.orderedBy}
      createdAt={item.createdAt}
      updatedAt={item.updatedAt}
      city={item.city}
      street={item.street}
      number={item.number}
      navigation={navigation}
      fetchFunction={onPressAddToCart}
      buttonTitle="Add to cart"
      routeAddress="Store"
      token={token}
      privilege={privilege}
      email={email}
    />
  );

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item._id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default StoreScreen;
