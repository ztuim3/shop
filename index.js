/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './components/App';
import {name as appName} from './app.json';
import {AppProvider} from './appContext';

AppRegistry.registerComponent(appName, () => App);
